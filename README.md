# Purge SNS

The Purge SNS module can send tag invalidations to
[Amazon SNS](https://aws.amazon.com/sns/) using the Drupal
[Purge module](https://www.drupal.org/project/purge). It can also
subscribe/receive SNS messages and invalidate those cache tags on the site.

Typically this is used for a multi-region site which has the same database,
but using Redis or Memcache for caching which has a separate instance on
each region.

## Installation

```
$ composer require drupal/purge_sns
```

## Configuration

### Sending SNS cache tag invalidations

The purge module should be enabled and configured with the "SNS Purger"
enabled at `admin/config/development/performance/purge`.

In your `settings.php` or `settings.local.php` file, the following settings
should be set for sending the cache tag invalidations on the site to SNS:

```
$settings['purge.purge_sns.aws_topic'] = 'arn:aws:sns:us-east-1:000000000000:purge-sns-topic';

// These are optional if you are using role or IAM-based authentication.
$settings['purge.purge_sns.aws_key'] = YOUR_AWS_ACCESS_KEY_ID;
$settings['purge.purge_sns.aws_secret'] = YOUR_AWS_SECRET_ACCESS_KEY;

// These are optional and the following values are the defaults:
$settings['purge.purge_sns.aws_region'] = 'us-east-1';
$settings['purge.purge_sns.aws_api_version'] = 'latest';
```

We do not recommend hard-coding your AWS key and secret information in your
settings files. Using environment variables for these settings is recommended:

```
$settings['purge.purge_sns.aws_key'] = getenv('AWS_ACCESS_SNS_KEY');
$settings['purge.purge_sns.aws_secret'] = getenv('AWS_ACCESS_SNS_SECRET');
```

For local environments you could use [phpdotenv](https://github.com/vlucas/phpdotenv)
to continue using environment variables on your local, or a
[local credentials file](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_profiles.html)
which would avoid needing to set the aws_key and aws_secret values.

### Configuration for subscribing to SNS cache tag invalidations

The amazon_sns module should be enabled. The purge module does not need to be
enabled for subscribing sites.

1. You will need three required pieces of information. These could be created
   by you or provided to you. The given AWS credentials should have the ability
   to subscribe to the SNS topic.
   1. SNS Topic ("Topic ARN")
   1. AWS access key
   1. AWS secret key
1. Subscribe your site to the SNS topic with the AWS CLI tool:
   1. `$ brew install awscli`
   1. `$ aws configure # To set your API keys`
   1. `$ aws sns subscribe --topic-arn THE-SNS-TOPIC --protocol https --notification-endpoint https://site-domain.com/_amazon-sns/notify`
   1. Verify the subscription is complete in the Drupal site logs.

In your `settings.php` or `settings.local.php` file, the following settings
should be set for subscribing to SNS cache tag invalidations on the site:

```
$settings['purge.purge_sns.subscribe_aws_topic'] = 'arn:aws:sns:us-east-1:000000000000:purge-sns-topic';
```

### Preventing Circular Cache Clearing

If a site is both sending its cache tag invalidations to SNS using the Purge
module and is also subscribed to the same SNS topic, this could create a
circular loop. If this configuration is detected, the SNS subscriber will
refuse to listen to any SNS cache tag invalidation messages.
