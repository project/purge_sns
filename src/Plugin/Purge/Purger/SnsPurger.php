<?php

namespace Drupal\purge_sns\Plugin\Purge\Purger;

use Aws\Credentials\Credentials;
use Aws\Sns\SnsClient;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Site\Settings;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SNS Purger.
 *
 * @PurgePurger(
 *   id = "sns",
 *   label = @Translation("SNS Purger"),
 *   cooldown_time = 0.0,
 *   description = @Translation("SNS purger that publishes SNS messages for each given invalidation instruction."),
 *   multi_instance = FALSE,
 *   types = {"tag"},
 * )
 */
class SnsPurger extends PurgerBase {

  /**
   * The ARN for the topic.
   *
   * @var string
   */
  protected $topicArn;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->topicArn = Settings::get('purge.purge_sns.aws_topic');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    if (empty($this->topicArn)) {
      $this->updateInvalidations($invalidations, InvalidationInterface::NOT_SUPPORTED);
      return;
    }

    $tags = [];
    foreach ($invalidations as $invalidation) {
      $tags[] = $invalidation->getExpression();
    }

    try {
      $this->updateInvalidations($invalidations, InvalidationInterface::PROCESSING);
      $this->publishTagsToSns($tags);
      $this->updateInvalidations($invalidations, InvalidationInterface::SUCCEEDED);
    }
    catch (\Exception $e) {
      $this->logger()->error($e->getMessage());
      $this->updateInvalidations($invalidations, InvalidationInterface::FAILED);
    }
  }

  /**
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   * @param $state
   */
  protected function updateInvalidations(array $invalidations, $state) {
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($state);
    }
  }

  /**
   * Get an SNS client.
   *
   * @return \Aws\Sns\SnsClient
   *   The client.
   */
  protected function getClient() {
    $key = Settings::get('purge.purge_sns.aws_key');
    $secret = Settings::get('purge.purge_sns.aws_secret');
    $region = Settings::get('purge.purge_sns.aws_region', 'us-east-1');
    $version = Settings::get('purge.purge_sns.aws_api_version', 'latest');

    $client_config = [
      'region' => $region,
      'version' => $version,
    ];

    // Allow authentication with standard secret/key or IAM roles.
    if (isset($configuration['key']) && isset($configuration['secret'])) {
      $client_config['credentials'] = new Credentials($key, $secret);
    }

    return new SnsClient($client_config);
  }

  /**
   * Publish the passed cache tags to SNS.
   *
   * @param string[] $tags
   *   The tags to be published.
   */
  protected function publishTagsToSns(array $tags) {
    $msg = JSON::encode(['tags' => $tags]);

    $this->logger()->debug('Publishing message to SNS: @m, ARN: @a', ['@m' => $msg, '@a' => $this->topicArn]);

    $this->getClient()->publish([
      'TopicArn' => $this->topicArn,
      'Message' => $msg,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }
}
