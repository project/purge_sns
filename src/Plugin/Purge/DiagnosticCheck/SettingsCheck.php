<?php

namespace Drupal\purge_sns\Plugin\Purge\DiagnosticCheck;

use Drupal\Core\Site\Settings;

use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckBase;

/**
 * Verifies that SNS settings have been provided.
 *
 * @PurgeDiagnosticCheck(
 *   id = "sns",
 *   title = @Translation("SNS Purger"),
 *   description = @Translation("Verifies that SNS settings have been provided."),
 *   dependent_queue_plugins = {},
 *   dependent_purger_plugins = {"sns"}
 * )
 */
class SettingsCheck extends DiagnosticCheckBase {

  /**
   * {@inheritdoc}
   */
  public function run() {
    $topic = Settings::get('purge.purge_sns.aws_topic');
    if (empty($topic)) {
      $this->value = $this->t('SNS purging disabled');
      $this->recommendation = $this->t('The purge.purge_sns.aws_topic setting should be set to enable SNS purging.');
      return self::SEVERITY_INFO;
    }

    if (!preg_match('/^arn/', $topic)) {
      $this->recommendation = $this->t("The configured AWS topic does not look like an ARN.");
      return SELF::SEVERITY_ERROR;
    }

    if (Settings::get('purge.purge_sns.aws_topic') === Settings::get('purge.purge_sns.subscribe_aws_topic')) {
      $this->value = $this->t('Purger and subscriber SNS topic identical');
      $this->recommendation = $this->t('Purging and subscribing to the same SNS topic would create a circular loop, and will disable the subscriber from processing any cache tags.');
      return self::SEVERITY_ERROR;
    }

    $this->value = $this->t("Configured");
    return SELF::SEVERITY_OK;

  }
}
