<?php

namespace Drupal\purge_sns\Event;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Site\Settings;
use Drupal\Component\Serialization\Json;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\amazon_sns\Event\SnsMessageEvent;
use Drupal\amazon_sns\Event\SnsEvents;

/**
 * Class InvalidateCacheSnsSubscriber.
 *
 * Handles SNS notifications which indicate some elements of the cache should
 * be invalidated.
 *
 * @package Drupal\purge_sns\Event
 */
class InvalidateCacheSnsSubscriber implements ContainerInjectionInterface, EventSubscriberInterface {

  /**
   * Logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $invalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Psr\Log\LoggerInterface $logger */
    $logger = $container->get('logger.channel.purge_sns');
    return new static(
      $logger,
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * Constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $invalidator
   *   Cache tag invalidator.
   */
  public function __construct(LoggerInterface $logger, CacheTagsInvalidatorInterface $invalidator) {
    $this->logger = $logger;
    $this->invalidator = $invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SnsEvents::NOTIFICATION => 'handleNotification',
    ];
  }

  /**
   * Handle SNS cache invalidation notifications.
   *
   * @param \Drupal\amazon_sns\Event\SnsMessageEvent $event
   */
  public function handleNotification(SnsMessageEvent $event) {

    $message = $event->getMessage();
    $messageTopic = $message['TopicArn'];

    $subscribeTopic = Settings::get('purge.purge_sns.subscribe_aws_topic');

    if ($messageTopic !== $subscribeTopic) {
      return;
    }

    $this->logger->debug("Received cache invalidation message: @m", ["@m" => $message['Message']]);

    // Protect against being a publisher and subscriber
    $publishTopic = Settings::get('purge.purge_sns.aws_topic');
    if (!empty($publishTopic)) {
      $this->logger->warning("Subscribing and publishing cache invalidation messages on the same instance is not supported");
      return;
    }

    $decodedPayload = Json::decode($message['Message']);

    $tags = array_filter($decodedPayload['tags'], 'is_string');

    if (!empty($tags)) {
      $this->logger->debug("Invalidating @count tags: @t", ['@count' => count($tags), '@t' => implode(', ', $tags)]);
      $this->invalidator->invalidateTags($tags);
    }
  }
}
